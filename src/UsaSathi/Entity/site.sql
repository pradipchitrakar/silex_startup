
--
-- Table structure for table `identities`
--

CREATE TABLE `identities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_created_at` datetime NOT NULL,
  `identity_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `identities`
--

INSERT INTO `identities` (`id`, `user_id`, `token`, `device`, `token_created_at`, `identity_created_at`) VALUES
(2, 1, '5faeb1e3ac79c11cb510608b6aa0caaef78d4f1217e8cf63761865e4168b152519822c7', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', '2015-12-24 04:47:54', '2015-12-24 04:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `facebook_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_town_latitude` double DEFAULT NULL,
  `home_town_longitude` double DEFAULT NULL,
  `current_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_location_latitude` double DEFAULT NULL,
  `current_location_longitude` double DEFAULT NULL,
  `user_role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_provider` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL,
  `non_locked` tinyint(1) NOT NULL,
  `activation_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgot_password_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgot_password_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `facebook_id`, `first_name`, `middle_name`, `last_name`, `email`, `password`, `home_town`, `home_town_latitude`, `home_town_longitude`, `current_location`, `current_location_latitude`, `current_location_longitude`, `user_role`, `user_provider`, `activated`, `non_locked`, `activation_code`, `forgot_password_code`, `forgot_password_date`, `created_at`, `updated_at`, `last_login_at`) VALUES
(1, NULL, 'pradip', NULL, 'chitrakar', 'pradip_versionxp@yahoo.com', '5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg==', NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ROOT', NULL, 1, 1, NULL, NULL, NULL, '2015-12-23 04:00:00', '2015-12-23 04:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `identities`
--
ALTER TABLE `identities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FA392CE8A76ED395` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_1483A5E99BE8FD98` (`facebook_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `identities`
--
ALTER TABLE `identities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `identities`
--
ALTER TABLE `identities`
  ADD CONSTRAINT `FK_FA392CE8A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

