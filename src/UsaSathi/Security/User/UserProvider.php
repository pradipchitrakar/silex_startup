<?php

namespace UsaSathi\Security\User;

use UsaSathi\Security\User\ApiUserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;

class UserProvider implements ApiUserProviderInterface {

    private $em;
	private $jwt;

    public function __construct($em,$jwt) {
        $this->em  = $em;
		$this->jwt = $jwt;
    }

    public function loadUserByToken($token) {
        $identity = $this->em->getRepository('UsaSathi\Entity\Identity')
                ->findOneBy(array('token' => $token));
        if ($identity) {
            try {
                $this->jwt->decodeToken($token);
            } catch(\Exception $e) {
                throw new \Exception($e->getMessage(),403); 
            }
            return $identity->getUser();
        } else {
            return false;
        }
    }

    public function loadUserByUsername($username) {
        $user = $this->em->getRepository('UsaSathi\Entity\User')
                ->findOneBy(array('email' => $username));

        if (null === $user) {
            $message = sprintf(
                    'Unable to find an active User object identified by "%s".', $username
            );
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof \UsaSathi\Entity\User) {
            throw new UnsupportedUserException(
            sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return $class === 'UsaSathi\Entity\User';
    }

}

?>