<?php

/**
 * @author Pradip Chitrakar
 */
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
        'mysql_dev' => array(
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'usa_sathi_db',
            'user' => 'root',
            'password' => '',
            'port' => 3306,
            'charset' => 'utf8mb4'
        ),
        'mysql_prod' => array(
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'usa_sathi_db',
            'user' => 'root',
            'password' => '',
            'port' => 3306,
            'charset' => 'utf8mb4'
        ),
        'mysql_staging' => array(
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'usa_sathi_db',
            'user' => 'root',
            'password' => '',
            'port' => 3306,
            'charset' => 'utf8mb4'
        ),
    ),
));

$app->register(new Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), array(
    "orm.proxies_dir" => __DIR__ . "/../cache/{$app['env']}/doctrine/proxies",
    "orm.ems.default" => 'mysql',
    "orm.ems.options" => array(
        'mysql' => array(
            "connection" => 'mysql_dev',
            "mappings" => array(
                // Using actual filesystem paths
                array(
                    "type" => "annotation",
                    'use_simple_annotation_reader' => false,
                    "namespace" => "UsaSathi\Entity",
                    "path" => __DIR__ . "/../../src/UsaSathi/Entity",
                )
            )
        )
    ),
    "orm.custom.functions.numeric" => array(
        'acos' => 'DoctrineExtensions\Query\Mysql\Acos',
        'asin' => 'DoctrineExtensions\Query\Mysql\Asin',
        'atan2' => 'DoctrineExtensions\Query\Mysql\Atan2',
        'atan' => 'DoctrineExtensions\Query\Mysql\Atan',
        'cos' => 'DoctrineExtensions\Query\Mysql\Cos',
        'cot' => 'DoctrineExtensions\Query\Mysql\Cot',
        'hour' => 'DoctrineExtensions\Query\Mysql\Hour',
        'pi' => 'DoctrineExtensions\Query\Mysql\Pi',
        'power' => 'DoctrineExtensions\Query\Mysql\Power',
        'quarter' => 'DoctrineExtensions\Query\Mysql\Quarter',
        'rand' => 'DoctrineExtensions\Query\Mysql\Rand',
        'round' => 'DoctrineExtensions\Query\Mysql\Round',
        'sin' => 'DoctrineExtensions\Query\Mysql\Sin',
        'std' => 'DoctrineExtensions\Query\Mysql\Std',
        'tan' => 'DoctrineExtensions\Query\Mysql\Tan'
    ),
    "orm.custom.functions.datetime" => array(
        'date' => 'DoctrineExtensions\Query\Mysql\Date',
        'date_format' => 'DoctrineExtensions\Query\Mysql\DateFormat',
        'dateadd' => 'DoctrineExtensions\Query\Mysql\DateAdd',
        'datediff' => 'DoctrineExtensions\Query\Mysql\DateDiff',
        'day' => 'DoctrineExtensions\Query\Mysql\Day',
        'dayname' => 'DoctrineExtensions\Query\Mysql\DayName',
        'last_day' => 'DoctrineExtensions\Query\Mysql\LastDay',
        'minute' => 'DoctrineExtensions\Query\Mysql\Minute',
        'second' => 'DoctrineExtensions\Query\Mysql\Second',
        'strtodate' => 'DoctrineExtensions\Query\Mysql\StrToDate',
        'time' => 'DoctrineExtensions\Query\Mysql\Time',
        'timestampadd' => 'DoctrineExtensions\Query\Mysql\TimestampAdd',
        'timestampdiff' => 'DoctrineExtensions\Query\Mysql\TimestampDiff',
        'week' => 'DoctrineExtensions\Query\Mysql\Week',
        'weekday' => 'DoctrineExtensions\Query\Mysql\WeekDay',
        'year' => 'DoctrineExtensions\Query\Mysql\Year'
    ),
    "orm.custom.functions.string" => array(
        'binary' => 'DoctrineExtensions\Query\Mysql\Binary',
        'char_length' => 'DoctrineExtensions\Query\Mysql\CharLength',
        'concat_ws' => 'DoctrineExtensions\Query\Mysql\ConcatWs',
        'countif' => 'DoctrineExtensions\Query\Mysql\CountIf',
        'crc32' => 'DoctrineExtensions\Query\Mysql\Crc32',
        'degrees' => 'DoctrineExtensions\Query\Mysql\Degrees',
        'field' => 'DoctrineExtensions\Query\Mysql\Field',
        'find_in_set' => 'DoctrineExtensions\Query\Mysql\FindInSet',
        'group_concat' => 'DoctrineExtensions\Query\Mysql\GroupConcat',
        'ifelse' => 'DoctrineExtensions\Query\Mysql\IfElse',
        'ifnull' => 'DoctrineExtensions\Query\Mysql\IfNull',
        'match_against' => 'DoctrineExtensions\Query\Mysql\MatchAgainst',
        'md5' => 'DoctrineExtensions\Query\Mysql\Md5',
        'month' => 'DoctrineExtensions\Query\Mysql\Month',
        'monthname' => 'DoctrineExtensions\Query\Mysql\MonthName',
        'nullif' => 'DoctrineExtensions\Query\Mysql\NullIf',
        'radians' => 'DoctrineExtensions\Query\Mysql\Radians',
        'regexp' => 'DoctrineExtensions\Query\Mysql\Regexp',
        'replace' => 'DoctrineExtensions\Query\Mysql\Replace',
        'sha1' => 'DoctrineExtensions\Query\Mysql\Sha1',
        'sha2' => 'DoctrineExtensions\Query\Mysql\Sha2',
        'soundex' => 'DoctrineExtensions\Query\Mysql\Soundex',
        'uuid_short' => 'DoctrineExtensions\Query\Mysql\UuidShort'
    )
));
    
if (in_array($app['env'], array('prod', 'staging'))) {
    $app["orm.default_cache"] = array(
        'driver' => 'filesystem',
        'path' => __DIR__ . "/../cache/{$app['env']}/doctrine/cache"
    );
} else {
    $app["orm.default_cache"] = 'array';
}


$app->register(new Saxulum\Console\Silex\Provider\ConsoleProvider());
$app->register(new Saxulum\DoctrineOrmManagerRegistry\Silex\Provider\DoctrineOrmManagerRegistryProvider());


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../../resources/views',
    'twig.options' =>
    array(
        'cache' => __DIR__ . "/../cache/{$app['env']}"
    ),
    'twig.form.templates' => array(
        'form_div_layout.html.twig',
        'bootstrap_3_horizontal_layout.html.twig'
    )
));


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../../resources/views',
    'twig.options' =>
    array(
        'cache' => __DIR__ . "/../cache/{$app['env']}"
    ),
    'twig.form.templates' => array(
        'form_div_layout.html.twig',
        'bootstrap_3_horizontal_layout.html.twig'
    )
));

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
            $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
                // implement whatever logic you need to determine the asset path
                return sprintf('/usasathi/web/%s', ltrim($asset, '/'));
            }));

            return $twig;
        }));

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__ . "/../logs/{$app['env']}.log",
    'monolog.level' => $app['env'] == 'dev' ? "DEBUG" : "INFO"
));
