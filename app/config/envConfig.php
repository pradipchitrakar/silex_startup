<?php
/**
 * @author Pradip Chitrakar
 */
if (in_array($app['env'],array('prod','staging'))) {
	$app['debug']=false;
} else if ($app['env']=='dev') {
	$app['debug']=true;	
}