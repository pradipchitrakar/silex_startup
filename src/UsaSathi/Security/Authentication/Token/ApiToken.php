<?php
namespace UsaSathi\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
class ApiToken extends AbstractToken
{
    public $authenticationToken;
    function __construct(array $roles = array())
    {
        parent::__construct($roles);
        $this->setAuthenticated(count($roles) > 0);
    }
    public function getCredentials()
    {
        return '';
    }
}