This is a sample project based on Silex framework for stateless jwt token based authentication which uses symfony security component. Doctrine ORM is used for storing users and tokens.

![schema.png](https://bitbucket.org/repo/e65XXp/images/3851130738-schema.png)

1. To test the code please visit [http://pangraz.org/token_auth/silex_startup/web/](http://pangraz.org/token_auth/silex_startup/web/)
You will get username and password.

![1.png](https://bitbucket.org/repo/e65XXp/images/2791956358-1.png)


2. To get the access token,user has to supply username and password and send a post request at [http://pangraz.org/token_auth/silex_startup/web/index.php/api/v1/login](http://pangraz.org/token_auth/silex_startup/web/index.php/api/v1/login)
You will receive a json object with token and user key.

![2.png](https://bitbucket.org/repo/e65XXp/images/1414066139-2.png)


3. Copy the token and send the token under Authentication header to this page at [http://pangraz.org/token_auth/silex_startup/web/index.php/api/v1/test](http://pangraz.org/token_auth/silex_startup/web/index.php/api/v1/test)

![3.png](https://bitbucket.org/repo/e65XXp/images/3626637741-3.png)