<?php
/**
 * @author Pradip Chitrakar
 */
$loader=require_once __DIR__.'/../vendor/autoload.php';
\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(
    array($loader, 'loadClass')
);
$app = new Silex\Application();
require_once __DIR__.'/../app/config/setEnv.php';
require_once __DIR__.'/../app/config/envConfig.php';
require_once __DIR__.'/../app/config/registerServiceProvider.php';
require_once __DIR__.'/../app/config/services.php';
require_once __DIR__.'/../app/config/routes.php';
require_once __DIR__.'/../app/config/security.php';

$app->run();