<?php
namespace UsaSathi\Security\Token;

use Firebase\JWT\JWT;

class TokenManager {
	private $secretKey;
	private $signAlgorithm;
	
	public function __construct($secretKey,$signAlgorthim='HS512') {
		$this->secretKey  	 = $secretKey;
		$this->signAlgorithm =$signAlgorthim;
	}
	
	public function generateToken($tokenArray){
		if (!isset($tokenArray['iat'])) {
			throw new InvalidArgumentException('Missing array key for iat');
		} else if (!isset($tokenArray['jti'])) {
			throw new InvalidArgumentException('Missing array key for jti');
		} else if (!isset($tokenArray['iss'])) {
			throw new InvalidArgumentException('Missing array key for iss');
		} else if (!isset($tokenArray['nbf'])) {
			throw new InvalidArgumentException('Missing array key for nbf');
		} else if (!isset($tokenArray['exp'])) {
			throw new InvalidArgumentException('Missing array key for exp');
		}
		$jwt = JWT::encode(
			$tokenArray,      //Data to be encoded in the JWT
			$this->secretKey, // The signing key
			$this->signAlgorithm     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
		);
        return $jwt;
	}
	
	
	public function decodeToken($jwt){
		try {
			$token = JWT::decode($jwt, $this->secretKey, array('HS512'));
			return $token;
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		} 
	}
	
}