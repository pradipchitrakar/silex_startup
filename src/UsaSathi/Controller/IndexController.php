<?php

/**
 * @author Pradip Chitrakar
 */

namespace UsaSathi\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use UsaSathi\Entity\User;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;

class IndexController {

    public function index(Request $request, Application $app) {
        $user = new User();
        $encoder = $app['security.encoder_factory']->getEncoder($user);

        //compute the encoded password for foo
        $password = $encoder->encodePassword('foo', $user->getSalt());
        echo "username is pradip_versionxp@yahoo.com and password is foo";
        die;
//        $tokenId = base64_encode(mcrypt_create_iv(32));
//        $issuedAt = time();
//        $notBefore = $issuedAt;
//        $expire = $notBefore + 2592000;            // Adding 60 seconds
//        $serverName = $request->getHost();
//        $enc = $app['jwt.token_manager']->generateToken(
//                array(
//                    'iat' => $issuedAt, // Issued at: time when the token was generated
//                    'jti' => $tokenId, // Json Token Id: an unique identifier for the token
//                    'iss' => $serverName, // Issuer
//                    'nbf' => $notBefore, // Not before
//                    'exp' => $expire, // Expire
//                    'data' => array(// Data related to the signer user
//                        'user-agent' => $request->headers->get('user-agent'),
//                    )
//                )
//        );
//        try {
//            print_r($app['jwt.token_manager']->decodeToken($enc));
//            die;
//        } catch (\Exception $e) {
//            throw new CredentialsExpiredException($e->getMessage(), 401);
//        }
//        return $app['twig']->render('country/index.html.twig', array());
    }

    public function admin(Request $request, Application $app) {
        die('admin area');
    }

}
