<?php

/**
 * @author Pradip Chitrakar
 */


namespace UsaSathi\Controller\Provider;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class IndexControllerProvider implements  ControllerProviderInterface
{

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $routes= $app["controllers_factory"];
        $routes->get("/", "UsaSathi\\Controller\\IndexController::index")
                 ->bind('index');
		$routes->get("/api/v1/test", "UsaSathi\\Controller\\IndexController::admin")
                 ->bind('apiTest');
		
        return $routes;
    }
}