<?php

namespace UsaSathi\Security\Authentication;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use UsaSathi\Entity\Identity;

class ApiAuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface {

    protected $app;

    public function __construct($app) {
        $this->app = $app;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        $user = $token->getUser();
        $em = $this->app['doctrine']->getManager('mysql');
        $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + 2592000;            // Adding 60 seconds
        $serverName = $request->getHost();
        $key = $this->app['jwt.token_manager']->generateToken(
                array(
                    'iat' => $issuedAt, // Issued at: time when the token was generated
                    'jti' => $tokenId, // Json Token Id: an unique identifier for the token
                    'iss' => $serverName, // Issuer
                    'nbf' => $notBefore, // Not before
                    'exp' => $expire, // Expire
                    'data' => array(// Data related to the signer user
                        'user-agent' => $request->headers->get('user-agent'),
                    )
                )
        );

        $identity = new Identity();
        $identity->setToken($key);
        $identity->setUser($user);
        $em->persist($identity);
        $em->flush();
        $responseArray = array(
            'token' => $key,
            'user' => $user
        );
        return $this->app->json($responseArray);
    }

    public function onAuthenticationFailure(Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $exception) {
        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode(401);
        $response->setData(array('message' => $exception->getMessage()));
        return $response;
    }

}
