<?php
/**
 * @author Pradip Chitrakar
 */
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
) {
	$app['env']='prod';
} else {
	//change it to staging to simulate production environment
	$app['env']='dev';
}