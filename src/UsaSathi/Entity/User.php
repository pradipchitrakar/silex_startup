<?php

/**
 * User Model
 */

namespace UsaSathi\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class User implements AdvancedUserInterface, \Serializable,\JsonSerializable {

    const USER_TYPE_CUSTOMER = 'ROLE_CUSTOMER';
    const USER_TYPE_ADMIN = 'ROLE_ADMIN';
    const USER_TYPE_ROOT = 'ROLE_ROOT';
    const USER_PROVIDER_FACEBOOK = 'FACEBOOK';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $facebookId
     *
     * @ORM\Column(name="facebook_id", type="string", length=100, unique=true, nullable=true)
     * 
     */
    private $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=100, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
     */
    private $lastName;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=100, unique=true, nullable=false)
     * 
     */
    private $email;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="home_town", type="string", length=255, nullable=true)
     */
    private $homeTown;

    /**
     * @var float $homeTownLatitude
     *
     * @ORM\Column(name="home_town_latitude", type="float", nullable=true)
     */
    private $homeTownLatitude;

    /**
     * @var float $homeTownLongitude
     *
     * @ORM\Column(name="home_town_longitude", type="float", nullable=true)
     */
    private $homeTownLongitude;

    /**
     * @var string
     *
     * @ORM\Column(name="current_location", type="string", length=255, nullable=true)
     */
    private $currentLocation;

    /**
     * @var float $currentLocationLatitude
     *
     * @ORM\Column(name="current_location_latitude", type="float", nullable=true)
     */
    private $currentLocationLatitude;

    /**
     * @var float $currentLocationLongitude
     *
     * @ORM\Column(name="current_location_longitude", type="float", nullable=true)
     */
    private $currentLocationLongitude;

    /**
     * @var string $userRole
     *
     * @ORM\Column(name="user_role", type="string", length=50, nullable=false)
     */
    private $userRole;

    /**
     * @var string $userProvider
     *
     * @ORM\Column(name="user_provider", type="string", length=50, nullable=true)
     */
    private $userProvider;

    /**
     * @var boolean $activated
     *
     * @ORM\Column(name="activated", type="boolean", nullable=false)
     */
    private $activated;

    /**
     * @var boolean $nonLocked
     *
     * @ORM\Column(name="non_locked", type="boolean", nullable=false)
     */
    private $nonLocked;

    /**
     * @var string $activationCode
     *
     * @ORM\Column(name="activation_code", type="string", length=100, nullable=true)
     */
    private $activationCode;

    /**
     * @var string $forgotPasswordCode
     *
     * @ORM\Column(name="forgot_password_code", type="string", length=100, nullable=true)
     */
    private $forgotPasswordCode;

    /**
     * @var datetime $forgotPasswordDate
     *
     * @ORM\Column(name="forgot_password_date", type="datetime", nullable=true)
     */
    private $forgotPasswordDate;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime",nullable=false)
     */
    private $updatedAt;

    /**
     * @var datetime $lastLoginAt
     *
     * @ORM\Column(name="last_login_at", type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\OneToMany(targetEntity="Identity", mappedBy="user",cascade={"remove"})
     */
    private $identities;

    /**
     * Constructor
     */
    public function __construct() {
        $this->identities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set userRole
     *
     * @param string $userRole
     */
    public function setUserRole($userRole) {
        if (!in_array($userRole, array(self::USER_TYPE_CUSTOMER, self::USER_TYPE_ADMIN, self::USER_TYPE_ROOT))) {
            throw new \InvalidArgumentException("Invalid user role");
        }
        $this->userRole = $userRole;
    }

    /**
     * Get userRole
     *
     * @return string
     */
    public function getUserRole() {
        return $this->userRole;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     */
    public function setActivated($activated) {
        $this->activated = $activated;
    }

    /**
     * Get activated
     *
     * @return boolean
     */
    public function getActivated() {
        return $this->activated;
    }

    /**
     * Set nonLocked
     *
     * @param boolean $nonLocked
     */
    public function setNonLocked($nonLocked) {
        $this->nonLocked = $nonLocked;
    }

    /**
     * Get nonLocked
     *
     * @return boolean
     */
    public function getNonLocked() {
        return $this->nonLocked;
    }

    /**
     * Set activationCode
     *
     * @param string $activationCode
     */
    public function setActivationCode($activationCode) {
        $this->activationCode = $activationCode;
    }

    /**
     * Get activationCode
     *
     * @return string
     */
    public function getActivationCode() {
        return $this->activationCode;
    }

    /**
     * Set forgotPasswordCode
     *
     * @param string $forgotPasswordCode
     */
    public function setForgotPasswordCode($forgotPasswordCode) {
        $this->forgotPasswordCode = $forgotPasswordCode;
    }

    /**
     * Get forgotPasswordCode
     *
     * @return string
     */
    public function getForgotPasswordCode() {
        return $this->forgotPasswordCode;
    }

    /**
     * Set forgotPasswordDate
     *
     * @param datetime $forgotPasswordDate
     */
    public function setForgotPasswordDate($forgotPasswordDate) {
        $this->forgotPasswordDate = $forgotPasswordDate;
    }

    /**
     * Get forgotPasswordDate
     *
     * @return datetime
     */
    public function getForgotPasswordDate() {
        return $this->forgotPasswordDate;
    }

    public function getSalt() {
        return null;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set lastLoginAt
     *
     * @param datetime $lastLoginAt
     */
    public function setLastLoginAt($lastLoginAt) {
        $this->lastLoginAt = $lastLoginAt;
    }

    /**
     * Get lastLoginAt
     *
     * @return datetime
     */
    public function getLastLoginAt() {
        return $this->lastLoginAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtCreate() {
        if (!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtCreate() {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtUpdate() {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Add identity
     *
     * @param \UsaSathi\Entity\Identity $identity
     *
     * @return User
     */
    public function addIdentity(\UsaSathi\Entity\Identity $identity) {
        $this->identities[] = $identity;

        return $this;
    }

    /**
     * Remove identity
     *
     * @param \Pangraz\AppBundle\Entity\Identity $identity
     */
    public function removeIdentity(\UsaSathi\Entity\Identity $identity) {
        $this->identities->removeElement($identity);
    }

    /**
     * Get identities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdentities() {
        return $this->identities;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }
    
    public function getFacebookId() {
        return $this->facebookId;
    }

    public function getMiddleName() {
        return $this->middleName;
    }

    public function getHomeTown() {
        return $this->homeTown;
    }

    public function getHomeTownLatitude() {
        return $this->homeTownLatitude;
    }

    public function getHomeTownLongitude() {
        return $this->homeTownLongitude;
    }

    public function getCurrentLocation() {
        return $this->currentLocation;
    }

    public function getCurrentLocationLatitude() {
        return $this->currentLocationLatitude;
    }

    public function getCurrentLocationLongitude() {
        return $this->currentLocationLongitude;
    }

    public function getUserProvider() {
        return $this->userProvider;
    }

    public function setFacebookId($facebookId) {
        $this->facebookId = $facebookId;
    }

    public function setMiddleName($middleName) {
        $this->middleName = $middleName;
    }

    public function setHomeTown($homeTown) {
        $this->homeTown = $homeTown;
    }

    public function setHomeTownLatitude($homeTownLatitude) {
        $this->homeTownLatitude = $homeTownLatitude;
    }

    public function setHomeTownLongitude($homeTownLongitude) {
        $this->homeTownLongitude = $homeTownLongitude;
    }

    public function setCurrentLocation($currentLocation) {
        $this->currentLocation = $currentLocation;
    }

    public function setCurrentLocationLatitude($currentLocationLatitude) {
        $this->currentLocationLatitude = $currentLocationLatitude;
    }

    public function setCurrentLocationLongitude($currentLocationLongitude) {
        $this->currentLocationLongitude = $currentLocationLongitude;
    }

    public function setUserProvider($userProvider) {
        if ($userProvider && !in_array($userProvider, array(self::USER_PROVIDER_FACEBOOK))) {
            throw new \InvalidArgumentException("Invalid user provider");
        }
        $this->userProvider = $userProvider;
    }

    
    /*
     * Returns difference of forgotPasswordDate and current date in minutes
     */

    public function getForgotPasswordDateDiff() {
        if ($this->forgotPasswordDate) {
            $diff = $this->forgotPasswordDate->diff(new \DateTime());
            $minutes = $diff->days * 24 * 60;
            $minutes += $diff->h * 60;
            $minutes += $diff->i;
            return $minutes;
        }
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function jsonSerialize() {
        $arr=array(
            'id'            => $this->getId(),
            'email'      => $this->getEmail()
        );
        return $arr;
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    public function getUsername() {
        return $this->email;
    }

    public function isEnabled() {
        return $this->getActivated();
    }

    public function eraseCredentials() {
        
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return $this->getNonLocked();
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function getRoles() {
        return array($this->getUserRole());
    }

}
