<?php

namespace UsaSathi\Ratchet\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use UsaSathi\Ratchet\Client;

class Server extends Command {
    private $container;
    public function setContainer($app){
        $this->container=$app;
    }
    public function getContainer(){
        return $this->container;
    }
    protected function configure() {
        $this
                ->setName('ratchet:start')
                ->setDescription('Starts ratchet server')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $server = IoServer::factory(
                        new HttpServer(
                        new WsServer(
                        new Client()
                        )
                        ), 8080
        );
        $output->writeln("server started");
        $server->run();
    }

}
