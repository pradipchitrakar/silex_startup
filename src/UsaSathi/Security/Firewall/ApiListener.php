<?php
namespace UsaSathi\Security\Firewall;

use UsaSathi\Security\Authentication\Token\ApiToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class ApiListener implements ListenerInterface
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface
     */
    protected $authenticationManager;
    function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->headers->has('Authentication')) {
            $event->setResponse($this->createForbiddenResponse());
            return;
        }
        $authenticationToken = $request->headers->get('Authentication');
        $token = new ApiToken();
        $token->authenticationToken = $authenticationToken;
        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
            return;
        }
        catch(AuthenticationException $e) {
            $event->setResponse($this->createForbiddenResponse());
        }
        $event->setResponse($this->createForbiddenResponse());
    }
    /**
     * @return Response
     */
    protected function createForbiddenResponse()
    {
        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        return $response;
    }
}