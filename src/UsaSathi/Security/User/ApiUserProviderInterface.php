<?php
namespace UsaSathi\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

interface ApiUserProviderInterface extends UserProviderInterface
{
    /**
     * @param string $token
     * @return UserInterface
     */
    public function loadUserByToken($token);
} 