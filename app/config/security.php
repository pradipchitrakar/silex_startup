<?php

/**
 * @author Pradip Chitrakar
 */

$app['jwt.token_manager'] = $app->share(function () use ($app) {
    return new UsaSathi\Security\Token\TokenManager('nGh7b68ngeOw2Uu7gOJsXk531HNk5yxO');
});
$app['security.user_provider'] = $app->share(function () use ($app) {
    return new UsaSathi\Security\User\UserProvider($app['doctrine']->getManager('mysql'),$app['jwt.token_manager']);
});

$app['security.authentication_listener.factory.secure_api'] = $app->protect(function ($name, $options) use ($app) {
    // define the authentication provider object
    $app['security.authentication_provider.' . $name . '.secure_api'] = $app->share(function () use ($app) {
        return new UsaSathi\Security\Authentication\Provider\ApiProvider($app['security.user_provider']);
    });

    // define the authentication listener object
    $app['security.authentication_listener.' . $name . '.secure_api'] = $app->share(function () use ($app) {
        // use 'security' instead of 'security.token_storage' on Symfony <2.6
        return new UsaSathi\Security\Firewall\ApiListener($app['security.token_storage'], $app['security.authentication_manager']);
    });

    return array(
        // the authentication provider id
        'security.authentication_provider.' . $name . '.secure_api',
        // the authentication listener id
        'security.authentication_listener.' . $name . '.secure_api',
        // the entry point id
        null,
        // the position of the listener in the stack
        'pre_auth'
    );
});
$app['security.encoder.digest'] = $app->share(function ($app) {
    return new Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('bcrypt', false, 12);
});

$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'api_v1_login' => array(
            'pattern' => '^/api/v1/login$',
            'stateless' => true,
            'users' => $app['security.user_provider'],
            'form' => array(
                'check_path' => '/api/v1/login',
                'username_parameter' => 'username',
                'password_parameter' => 'password',
                'require_previous_session' => false
            )
        ),
        'api_v1_secure' => array(
            'pattern' => '^/api/.*',
            'stateless' => true,
            'secure_api' => true
        ),
    )
));

$app['security.authentication.success_handler.api_v1_login'] = $app->share(function ($app) {
    return new UsaSathi\Security\Authentication\ApiAuthenticationHandler($app);
});
$app['security.authentication.failure_handler.api_v1_login'] = $app->share(function ($app) {
    return new UsaSathi\Security\Authentication\ApiAuthenticationHandler($app);
});

