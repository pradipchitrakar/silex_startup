<?php

/**
 * @author Pradip Chitrakar
 */
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['env'] == 'dev') {
        return;
    } else {
        $acceptHeader = $app['request']->headers->get('Accept');
        if (in_array('application/json', $app['request']->getAcceptableContentTypes())) {
            $error = array(
                'error' => array(
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                )
            );
            return $app->json($error, (int) $e->getCode());
        }
        return $app['twig']->render('error.html.twig', array('code' => $code));
    }

    // ... logic to handle the error and return a Response
});
